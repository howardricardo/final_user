from rest_framework.permissions import BasePermission


class FinalUserPermissions(BasePermission):
    def has_permission(self, request, view):
        user = request.user
        method = request.method

        try:
            ...  # Lógica de acesso às rotas aqui...
        except:
            return False
