from django.contrib.auth.models import AbstractUser, BaseUserManager


class UserManager(BaseUserManager):
    use_in_migrations = True

    def _create_user(self, email, password, **extra_fields):
        if not email:
            raise ValueError("The given email must be set")
        email = self.normalize_email(email)
        user = self.model(email=email, **extra_fields)
        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_user(self, email, password=None, **extra_fields):
        extra_fields.setdefault("is_staff", False)
        extra_fields.setdefault("is_superuser", False)
        return self._create_user(email, password, **extra_fields)

    def create_superuser(self, email, password, **extra_fields):
        extra_fields.setdefault("is_staff", True)
        extra_fields.setdefault("is_superuser", True)

        return self._create_user(email, password, **extra_fields)


class FinalUserModel(AbstractUser):
    # Desabilita campos padrões do User Django ==========
    username = None
    user_permissions = None
    first_name = None
    last_name = None
    groups = None
    # ====================================================

    """
    Importante: Veja quais são os campos padrões do usuário do Django.
    Remova o que não for usar fazendo da mesma forma que fiz acima.
    """

    USERNAME_FIELD = "email"  # Seta o campo necessário para fazer login

    # Campos da tabela aqui...
