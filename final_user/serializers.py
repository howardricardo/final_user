from rest_framework import serializers
from .models import FinalUserModel


class LoginSerializer(serializers.Serializer):
    email = serializers.CharField()
    password = serializers.CharField()


class FinalUserSerializer(serializers.ModelSerializer):
    class Meta:
        model = FinalUserModel
        fields = '__all__'
