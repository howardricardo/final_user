import datetime

from django.contrib.auth import authenticate
from rest_framework import views, permissions, status, viewsets
from rest_framework.authentication import TokenAuthentication
from rest_framework.authtoken.models import Token
from rest_framework.response import Response

from final_user.serializers import LoginSerializer
from .serializers import FinalUserSerializer
from .models import FinalUserModel
from .permissions import FinalUserPermissions


class FinalUserViewSet(viewsets.ModelViewSet):
    queryset = FinalUserModel.objects.all()
    serializer_class = FinalUserSerializer
    permission_classes = [FinalUserPermissions]
    authentication_classes = [TokenAuthentication]  # Isso dá acesso ao request.user


class LoginAPIView(views.APIView):
    @staticmethod
    def post(request):
        serializers = LoginSerializer(data=request.data)
        serializers.is_valid(raise_exception=True)

        user = authenticate(
            email=request.data["email"], password=request.data["password"]
        )

        token = Token.objects.get_or_create(user=user)[0]
        now = datetime.datetime.now()

        united_states_timezone = 3

        if (
            now.year - token.created.year >= 1
            or now.day - token.created.day >= 1
            or now.hour - (token.created.hour - united_states_timezone) >= 1
        ):
            token.delete()
            token = Token.objects.get_or_create(user=user)[0]

        return Response(
            {
                "token": token.key
            },
            status=status.HTTP_200_OK,
        )
