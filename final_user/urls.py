from django.urls import path
from .views import LoginAPIView

urlpatterns = [
    path(r"login/", LoginAPIView.as_view())
]
